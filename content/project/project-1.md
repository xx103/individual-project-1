
+++
title = "Statistics and Data Science with Real-World Case Studies"
date = 2022-07-01  # Start date of the project
[taxonomies]
categories = ["Data Science", "Research"]
tags = ["R", "Data Analysis", "Real Estate", "Yale"]
+++

As a Group Leader and under the supervision of Professor John Emerson from Yale University, I used R language to assess 19,621 HTML files from Vision, analyzed 6,931 house properties in Branford, Connecticut.

- Organized housing characteristics data such as living area, building percent good, and heating system via assembly and cleaning.
- Utilized advanced data processing techniques to perform regression analyses and construct models, unveiling factors impacting appraised values of Branford houses.
- Authored an article titled "An Analysis of Significant Factors that Affect Branford Houses’ Appraised Value," to be published at the 2nd International Conference on Business and Policy Studies (CONF-BPS 2023) in November 2023.

---
