+++
title = "IDS 721 Project 6: Rust Lambda Logging and Tracing"
date = 2024-03-22
[taxonomies]
categories = ["Cloud Computing", "Mini-Project"]
tags = ["Rust", "AWS Lambda", "AWS X-Ray", "CloudWatch", "Logging", "Tracing"]
+++

## Project Overview

This project demonstrates how to instrument a Rust Lambda function for logging and tracing, integrating it with AWS services like X-Ray and CloudWatch. The Lambda function calculates an overall grade based on input grades and weights for quizzes, homework, and exams.

For more details, visit the [project repository on GitLab](https://gitlab.com/xx103/week-6-mini-project).


## Setup Instructions

### Prerequisites

- Rust installed on your system.
- AWS CLI configured with necessary permissions (AWSLambda_FullAccess, IAMFullAccess, AWSXrayFullAccess).
- Cargo Lambda installed for Rust Lambda development.

### 1. IAM User Setup

- Log in to your AWS account, navigate to the IAM (Identity and Access Management) console, and create a new user.
- Attach `AWSLambda_FullAccess`, `IAMFullAccess`, and `AWSXrayFullAccess` policies.
- Note down the `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

### 2. Project Setup

- Create a new project with `cargo lambda new project_name`.
- Update `main.rs` and `Cargo.toml`.
- Add a `.env` file with your AWS credentials and region, and ensure it's listed in `.gitignore`.
- Export your AWS credentials and region in your terminal session.
- Run `cargo lambda watch` in the terminal to test your Lambda function.
- Run the following command to invoke your Lambda function locally:
    ```
    cargo lambda invoke --data-ascii '{ "quiz1_grade": 85.0, "quiz2_grade": 90.0, "hw_grade": 80.0, "exam_grade": 95.0, "quiz_weight": 0.25, "hw_weight": 0.25, "exam_weight": 0.5 }'
    ```

- Deploy your Lambda function with:
    ```
    cargo lambda build --release
    cargo lambda deploy
    ```

### 3. AWS Lambda Setup

- Navigate to the AWS Management Console and open the Lambda service.
- Locate your deployed Lambda function in the dashboard and select it.
- Within the function's detail page, go to the "Configuration" tab.
- In the Additional monitoring tools section, enable `X-Ray active tracing` and `Lambda Insights enhanced monitoring`.
- Run the following command to test remote invocation:
    ```
    cargo lambda invoke --remote rust_lambda_grade_calculator --data-ascii '{ "quiz1_grade": 85.0, "quiz2_grade": 90.0, "hw_grade": 80.0, "exam_grade": 95.0, "quiz_weight": 0.25, "hw_weight": 0.25, "exam_weight": 0.5 }'
    ```
- Test the Lambda function from the AWS Console in the "Test" tab.

### 4. API Gateway Setup

- Access the API Gateway service from the AWS Management Console.
- Create a new `REST API`.
- Include an `ANY` method for your API.
- Deploy your API to obtain an Invoke URL.
- Test your API Gateway using terminal commands.

## Lambda `rust_lambda_grade_calculator` Function

![Function overview](/images/screenshot13.png)

## Test the API Gateway Using Curl

![Function overview](/images/screenshot14.png)

## Test Lambda Function in AWS

![Function overview](/images/screenshot15.png)

![Function overview](/images/screenshot16.png)

## CloudWatch Traces With Remote Invoke in Local Terminal and AWS

![Function overview](/images/screenshot17.png)

![Function overview](/images/screenshot18.png)

## Trace-level Loggings

![Function overview](/images/screenshot19.png)

![Function overview](/images/screenshot20.png)
