+++
title = "IDS721 Project 3: S3 Bucket Creation"
date = 2024-03-08
[taxonomies]
categories = ["Cloud Computing", "Mini-Project"]
tags = ["AWS", "AWS CDK", "S3 Bucket", "AWS Cloud9"]
+++

## Overview
This project aims to create an S3 bucket using AWS CDK (Cloud Development Kit) along with incorporating bucket properties like versioning and encryption. It utilizes AWS CodeWhisperer for generating CDK code efficiently and deploys the infrastructure through AWS Cloud9.

For more details, visit the [project repository on GitLab](https://gitlab.com/xx103/idsminiproject3).

## Requirements
- AWS Account with necessary permissions.
- Access Key ID and Secret Access Key for programmatic access.
- Amazon CodeCatalyst account.

## Steps to Setup and Deploy

### 1. IAM User Setup
1. Log in to AWS account and navigate to Identity and Access Management (IAM).
2. Click on `Users` in the left dashboard and `create a new user`.
3. Attach the following permission policies directly:
    - `AmazonS3FullAccess`
    - `AWSLambda_FullAccess`
    - `IAMFullAccess`
4. Add the following inline policies:
    - `CloudFormation`
    - `Systems_Manager`
5. Save the generated `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.

### 2. Project Setup with AWS CodeCatalyst
1. Sign in to Amazon CodeCatalyst.
2. Create a new project starting from scratch, providing a project name.
3. `Create a dev environment` and open it with AWS Cloud9.

### 3. AWS Cloud9 Configuration
1. In the Cloud9 terminal, configure AWS CLI by running `aws configure` and input the saved `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY`.
2. Create a directory for the project by running `mkdir "project name"` and navigate into it using `cd "project name"`.

### 4. Project Initialization and Code Generation
1. Initialize the project with the CDK template using the following command:
    ```
    cdk init app --language typescript
    ```
2. Utilize AWS CodeWhisperer to facilitate code generation and revision.
3. In `lib/idsminiproject3-stack.ts`, use //create an S3 bucket and // Make an S3 bucket with versioning enabled and server-side encryption.
4. In `bin/idsminiproject3.ts`, use // define and deploy an S3 bucket through necessary environment variables

### 5. Build and Deployment
1. In the Cloud9 terminal, run the following commands:
    ```
    npm run build
    cdk synth
    cdk deploy
    ```
2. Verify whether the S3 stack has been deployed successfully.

## Additional Notes
- Ensure proper cleanup after testing to avoid unnecessary costs.
- Refer to AWS CDK documentation for further customization and advanced features.

## Screenshot of S3 Bucket
![Function overview](/images/screenshot3.png)
![Function overview](/images/screenshot4.png)


