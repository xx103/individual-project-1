+++
title = "IDS721 Project 2: Diamond Data Analysis with Rust and AWS Lambda"
date = 2024-03-08
[taxonomies]
categories = ["Data Science", "Mini-Project"]
tags = ["Rust", "AWS Lambda", "Data Analysis"]
+++

# Overview

This project demonstrates how to perform data analysis on a dataset of diamonds, using Rust with the Polars library for efficient data manipulation and AWS Lambda for serverless execution. The program filters, groups, and aggregates data, focusing on diamonds with specific characteristics.

For more details, visit the [project repository on GitLab](https://gitlab.com/xx103/week-2-mini-project).

The program operates in several steps:

1. **Filtering:** Selects diamonds based on their 'carat' value being greater than a specified threshold and their 'color' equal to "D".
2. **Grouping:** Groups the filtered data by the 'clarity' attribute.
3. **Aggregation:** Calculates the mean of 'table' (referred to as "width_mean"), 'depth' (referred to as "height_mean"), and 'price' (referred to as "price_mean") for each clarity group.

The output is a matrix with clarity types as rows and aggregated measures as columns.

## Project Structure

- `lib.rs`: Contains the core functionality for reading, filtering, grouping, and aggregating the dataset.
- `main.rs`: Integrates the Rust application with AWS Lambda, defining an asynchronous handler function for processing Lambda events.
- `Cargo.toml`: Specifies the Rust package configurations, including dependencies necessary for the project.
- `Makefile`: Provides convenient commands for building, deploying, and testing the application.

## Setup

### Requirements

- Rust and Cargo: Ensure Rust and Cargo are installed on your system. Use the `rust-version` target in the `Makefile` to verify your installation.
- AWS CLI: Installed and configured with appropriate access rights for deploying and invoking AWS Lambda functions.
- AWS Lambda: Setup with an execution role that has permissions to run Lambda functions and access necessary AWS resources.

### Installation

1. **Clone the Repository**: Start by cloning this repository to your local machine.

2. **Install Dependencies**: Run `cargo build` to install the necessary Rust dependencies listed in `Cargo.toml`.

3. **Configure AWS CLI**: Ensure AWS CLI is configured with your credentials and default region. Use `aws configure` to set up.

## Usage

### Local Testing

- **Format Code**: `make format` to format the Rust code.
- **Lint**: `make lint` to run clippy, Rust's linter, and catch common mistakes.
- **Run Tests**: `make test` to execute unit tests.
- **Local Invocation**: `make invoke` to invoke the function locally with a test payload.

### Deployment

- **Build for AWS Lambda**: `make build` to compile the application for AWS Lambda's execution environment.
- **Deploy to AWS Lambda**: `make deploy` to deploy your application to AWS Lambda.
- **Invoke on AWS**: `make aws-invoke` to invoke your Lambda function in the cloud with a test payload.

### Development Workflow

- **Watch for Changes**: `make watch` to automatically rebuild your project upon source code changes.
- **Run Locally**: `make run` to execute the application locally.
- **Release**: `make release` to build a release version of your application.

## Output Explanation

The output matrix will have a shape indicating the number of clarity types and columns for clarity, width_mean, height_mean, and price_mean. For example, filtering diamonds with 'carat' greater than 0.1 and 'color' equal to "D" might produce a DataFrame with 5 rows and 4 columns, each row representing a clarity type and columns representing aggregated measures.

## AWS Example 

![Function overview](/images/screenshot1.png)
![Function overview](/images/screenshot2.png)


