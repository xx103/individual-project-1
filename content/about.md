+++
title = "About Me"
date = 2024-01-29  
template = "non-post-page.html"
+++

I am a passionate and dedicated professional with a strong background in statistical science and a flair for data analysis. Currently based in Durham, NC, I am on a journey to explore and contribute to the field of data science and finance. Let me walk you through my educational journey, core competencies, professional experiences, and research endeavors.

## Education

**Duke University, Durham, NC**  
*M.S. in Statistical Science, Anticipated May 2025*  
Key Courses: Predictive Modeling and Statistical Learning, Programming for Statistical Science, Probabilistic Machine Learning, Bayesian Statistics

**University of California, Los Angeles, CA**  
*B.S. in Mathematics & Economics, Graduated June 2023*  
Courses Included: Intro to Programming, Probability, Statistics, Linear Algebra, Real Analysis, Optimization  
Cumulative GPA: 3.90/4.00, Cum Laude

## Core Competencies

My academic and professional experiences have equipped me with a robust set of skills, including:

- Data Analysis & Visualization
- Statistical Modeling & Regression Analysis
- Predictive Modeling & Quantitative Research
- Machine Learning & Model Validation

Technical Proficiency: Proficient in R, SQL, Python, C++, MATLAB, Mathematica, and Microsoft Office Suite.  
Languages: Fluent in English and Chinese (Mandarin).

## Experience and Impact

**Bank of China Co., Ltd., Investment Banking Department**  
*Finance Intern*  
Shanghai, China (July 2021 – September 2021)  
- Conducted financial ratio analysis to evaluate company performance.
- Performed industry research using PEST and Five-Force Model.
- Assisted in preparing investment analysis reports.

**Suzhou Jinding Accounting Firm Co., Ltd.**  
*Accounting Intern*  
Suzhou, China (July 2020 – August 2020)  
- Supported a loan deduction initiative and managed financial metrics.
- Improved client communication and financial document interpretation skills.

---

Feel free to reach out via email at xx103@duke.edu. I'm always open to discussing opportunities, collaborations, or just exchanging ideas in the realms of data science and finance.
