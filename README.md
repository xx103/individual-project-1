# Individual Project 1: Personal Website 

# Xingzhi Xie's Personal Website Link

It is hosted on GitLab with the link: https://individual-project-1-xx103-0fc97b0dfb1b683262cdf2c2414b99e4afc5.gitlab.io/

![Video Demo](https://gitlab.com/xx103/individual-project-1/-/wikis/uploads/18e617db790705b48976e741d0d8c165/video1609600572.mp4)
## Home Page

The home page serves as an introduction to who I am and what this site is about. It highlights my current academic pursuit of a Master's in Statistical Science at Duke University and my background in Mathematics & Economics from UCLA. Here, visitors can get a quick overview of my skills, experiences, and the various sections of the website.

![Function overview](/screenshots/screenshot1.png)


## About

The about section offers a deeper dive into my background. It outlines my education, core competencies, and professional experiences in more detail. From statistical modeling to machine learning, this section covers the breadth of my expertise and my academic accolades.

- **Education**: Details on my coursework and accomplishments at Duke University and UCLA.
- **Core Competencies**: A list of the analytical and technical skills I've developed.
- **Experience and Impact**: Insight into my roles at the Bank of China and Suzhou Jinding Accounting Firm.

![Function overview](/screenshots/screenshot2.png)


## Project

This section showcases research projects that I have conducted over time. It features detailed case studies and the application of data science in various contexts. Two highlighted projects include:

- **Culinary Market Analysis Tool Leveraging Yelp Data**: A project that entailed developing an R-based Shiny dashboard to analyze over 1.16 million Yelp reviews.
- **Statistics and Data Science with Real-World Case Studies**: This project, supervised by Professor John Emerson from Yale University, involved analyzing housing data in Branford, Connecticut.

![Function overview](/screenshots/screenshot3.png)
![Function overview](/screenshots/screenshot4.png)


## Mini-Projects

The mini-projects section contains concise IDS721 projects that demonstrate specific skills or concepts in cloud computing and data science. These projects are smaller in scope but crucial in showcasing my ability to apply theoretical knowledge to practical applications. Examples include:

![Function overview](/screenshots/screenshot5.png)
![Function overview](/screenshots/screenshot6.png)
![Function overview](/screenshots/screenshot7.png)
![Function overview](/screenshots/screenshot8.png)



